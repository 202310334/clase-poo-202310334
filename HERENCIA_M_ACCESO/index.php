<?php

class EjemploModificadores{

    public function metodo1(){
        echo "Este es el metodo 1 ";
    }

    private function metodo2(){
        echo "Este es el metodo 2 ";
    }

    protected function metodo3(){
        echo "Este es el metodo 3 ";
    }

    public function accesoMetodo2(){
        $this->Metodo2();
    }    
}

class Hijo extends EjemploModificadores{
public function accesoMetodo3(){
    $this->Metodo3();
}

}
$obj = new Hijo;

$obj->metodo1();
//$obj->metodo2();//

$obj->accesometodo3();

$obj2 = new EjemploModificadores;
$obj->accesoMetodo2();






?>