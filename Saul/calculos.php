<?php

class Calculos{

    //Atributo Operaciones a Realizar
    public $CalculoRealizar = "";

    //Constructor
    public function __construct ($CalculoRealizar){
        $this->CalculoRealizar = $CalculoRealizar;
    }


    //Metodo cuadrado con parametros digito 1  
    private function Cuadrado($digito1){
      return $digito1 * $digito1;
    }
    //Metodo Cubo con parametros digito 1 
    private function Cubo($digito1){
        return $digito1 * $digito1 * $digito1;
    }
    //Metodo Cuarta con parametros digito 1
    private function Cuarta($digito1){
        return $digito1 * $digito1 * $digito1 * $digito1;
    }
    //Metodo Quinta con parametros digito 1
    private function Quinta($digito1){
        return $digito1 * $digito1 * $digito1 * $digito1 * $digito1;
    }
    
    //Metodo que ejecuta los metodos en base a la propiedad $CalculoRealizar y tiene 1 parametro (digito1)    
    public function ResultadoCalculo($digito1){

        switch ($this->CalculoRealizar) {
            case 'cuadrado':
                return $this->Cuadrado($digito1);
                break;
            case 'cubo':
                return $this->Cubo($digito1);
                break;
            case 'cuarta':
                return $this->Cuarta($digito1);
                break;
            case 'quinta':
                return $this->Quinta($digito1);
                break;    
            
            default:
                return 'Operacion a realizar no definida';
                break;
        }
   
    }
}


?>