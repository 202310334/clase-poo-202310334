<?php

class Automovil{

    public $motor = 0;
    public $arrastre = 0;
    public $torque = 0;


    public function setmotor($Litros_motor){
        $this->motor = $Litros_motor;
    }
    
    public function getmotor(){
        return $this->motor."Litros";
    }
    
    public function setarrastre($kg_arrastre){
        $this->arrastre = $kg_arrastre;
    }
    
    public function getarrastre(){
        return $this->arrastre."kg";
    }
    
    public function settorque($Torque_torque){
        $this->torque = $Torque_torque;
    }
    
    public function gettorque(){
        return $this->torque."Torque";
    }
}


$objFordRanger = new Automovil;

$objFordRanger->setmotor(2.5);
$objFordRanger->setarrastre(2500);
$objFordRanger->settorque(250);

echo "Motor Ford Ranger :" . $objFordRanger->getmotor()."<br>";
echo "Arrastre Ford Ranger :" . $objFordRanger->getarrastre()."<br>";
echo "Torque Ford Ranger :" . $objFordRanger->gettorque()."<br>";

?>