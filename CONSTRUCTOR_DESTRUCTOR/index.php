<?php

class Ejemplo{

    public $AtributoEjemplo;

    function __construct(){
        $this->AtributoEjemplo = 23;
        echo "Este es el constructor";
    }

    function __destruct(){
        $this->AtributoEjemplo = null;
        echo "Este es el destructor";
    }
}

$obj = new Ejemplo();
echo $obj->AtributoEjemplo;


?>