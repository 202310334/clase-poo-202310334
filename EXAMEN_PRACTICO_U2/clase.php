<?php

class Calculos{

    //Atributo Operaciones a Realizar
    public $CalculoRealizar = "";

    //Constructor
    public function __construct ($CalculoRealizar){
        $this->CalculoRealizar = $CalculoRealizar;
    }


    //Metodo cuadrado con parametros digito 1  
    private function Cuadrado($digito1){
      return $digito1 * $digito1;
    }
    
    //Metodo que ejecuta los metodos en base a la propiedad $CalculoRealizar y tiene 1 parametro (digito1)    
    public function ResultadoCalculo($digito1){

        switch ($this->CalculoRealizar) {
            case 'cuadrado':
                return $this->Cuadrado($digito1);
                break;
            
            default:
                return 'Operacion a realizar no definida';
                break;
        }
   
    }
}


?>