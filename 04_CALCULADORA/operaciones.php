<?php

//Clase Operaciones
class Operaciones{

    //Atributo Operaciones a Realizar
    public $OperacionRealizar = "";

    //Constructor
    public function __construct ($OperacionRealizar){
        $this->OperacionRealizar = $OperacionRealizar;
    }


    //Metodo Suma con parametros a y b
    public function Suma($a,$b){
      return $a + $b;
    }
    //Metodo Resta con parametros a y b
    public function Resta($a,$b){
        return $a-$b;
    }
    //Metodo Divicion con parametros a y b
    public function Divicion($a,$b){
        return $a/$b;
    }
    //Metodo Multiplicacion con parametros a y b
    public function Multiplicacion($a,$b){
        return $a*$b;
    }
    
    //Metodo que ejecuta los metodos en base a la propiedad $OperacionRealizar y tiene dos parametros $a y $b
    public function ResultadoOperacion($a,$b){

        switch ($this->OperacionRealizar) {
            case 'suma':
                return $this->Suma($a,$b);
                break;
            case 'resta':
                return $this->Resta($a,$b);
                break;
            case 'divicion':
                return $this->Divicion($a,$b);
                break;
            case 'multiplicacion':
                return $this->Multiplicacion($a,$b);
                break;    
            
            
        }
   
    }
 

}






?>