<?php

interface OperacionesGenerales{
    public function redondeo($monto);
    public function calcularIVA($monto);
}
class Puntodeventa implements OperacionesGenerales{
    public function redondeo($monto){
        return round($monto);
    }

    public function calcularIVA($monto){
        return $iva = $monto * .16;
    }
}
?>