<?php
//Creasion de la clase abstracta//
abstract class Transporte{
    //Metodo abstracto//
    abstract protected function mantenimiento();
}
//Clase heredada llamada avion//
class Avion extends Transporte{
    //Metodo heredado pero con  acceso publico para poder verlo//
    public function mantenimiento(){
        //un echo para mandar a imprimir los mantenimeintos de un Avion//
        echo "<b>Mantenimiento basico de un avion:</b> <br /> 
        Las inspecciones periódicas. <br /> 
        Las revisiones pre-vuelo. <br /> 
        Monitoreo de rendimiento. <br /> 
        Registro de ajustes y tareas en el equipo. <br />
        Realización de modificaciones y reparaciones. <br />";
    }    
}
//Clase heredada llamada Automovil//
class Automovil extends Transporte{    
    public function mantenimiento(){
        echo "<b>Mantenimiento basico a un automovil:</b> <br /> 
        Revision o cambio de aceite. <br /> 
        Chequeo de llantas. <br /> 
        Revision de frenos. <br /> 
        Amartiguadores. <br /> 
        Revision de bateria. <br /> 
        Banda o correa del motor. <br />";
    }
}
//Clase heredada llamada Motocicleta//
class Motocicleta extends Transporte{
    public function mantenimiento(){
        echo "<b>Mantenimiento basico de una motocicleta:</b> <br /> 
        Comprobar el nivel de aceite del carter. <br /> 
        Comprobar las ruedas. <br /> 
        Comprobar la bateria. <br /> 
        Comprobar las pastillas de freno y cadena. <br /> 
        Limpiar y lubricar. <br />";
    }
}
//Clase heredada llamada Barco//
class Barco extends Transporte{
    public function mantenimiento(){
        echo "<b>Mantenimiento basico de un barco:</b> <br />  
        Varada anual del barco. <br /> 
        Realizar los cambios recomendados por el fabricante. <br /> 
        Revisión de los elementos no mecánicos. <br /> 
        Elementos de seguridad y de higiene. <br />"; 
    }
}
//Clase heredada llamada Submarino//
class Submarino extends Transporte{
    public function mantenimiento(){
        echo "<b>Mantenimiento basico de un submarino:</b> <br /> 
        Limpieza de casco. <br /> 
        Pulido de helices. <br /> 
        Limpieza de mallas de admision de agua. <br /> 
        Cambio de ánodos de sacrificio. <br /> 
        Limpieza y chatarreo marino.";
    }
}
//Instanciacion de la clase Avion//
$obj = new Avion();
//Hacer referencia al metodo de la clase para poder imprimir//
$obj->mantenimiento();
//Instanciacion de la clase Automovil//
$obj2 = new Automovil();
//Hacer referencia al metodo de la clase para poder imprimir//
$obj2->mantenimiento();
//Instanciacion de la clase Motocicleta//
$obj3 = new Motocicleta();
//Hacer referencia al metodo de la clase para poder imprimir//
$obj3->mantenimiento();
//Instanciacion de la clase Barco//
$obj4 = new Barco();
//Hacer referencia al metodo de la clase para poder imprimir//
$obj4->mantenimiento();
//Instanciacion de la clase Submarino//
$obj5 = new Submarino();
//Hacer referencia al metodo de la clase para poder imprimir//
$obj5->mantenimiento();
?>