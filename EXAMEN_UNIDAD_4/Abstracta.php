<?php
abstract class ToyotaTacoma{
    abstract protected function Caracteristicas();
}

class Motor extends ToyotaTacoma{
    public function Caracteristicas(){ 
        echo "<b>Las caracteristicas del motor son:</b> <br />
        6 cilindros. <br /> 
        3.5 litros. <br /> 
        inyecccion directa. <br />
        6 velocidades. <br /> ";
    }
}
$obj = new Motor();
$obj->Caracteristicas();
?>        