<?php
interface Calculadora{
    public function suma($a,$b);
    public function resta($a,$b);
}
class Resultados implements Calculadora{
    public function suma($a,$b){
        return $a+$b;
    }

    public function resta($a,$b){
        return $a-$b;
    }
}

$obj = new Resultados();
echo "<h1>Resultado de suma: " .$obj->suma(10,10)."</h1>";
echo "<h1>Resultado de resta: " .$obj->resta(5,1)."</h1>";



?>