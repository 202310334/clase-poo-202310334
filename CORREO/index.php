<?php
//Clase llamada correo electronico//
class CorreoElectronico{

    //Atributos//
    private $contraseña = "";
    public $correo = "";
    protected $recuperarContraseña = "";

    //Metodos de private, public y protected//
    //Que en este metodo como es private no se puede ver fuera de la clase//
    private function contraseña(){
        return $this->contraseña; //Proporcionar la contraseña//
    }

    public function correo(){
        return $this->correo = "22saulramirez@outlook.com";
    }
    //En este metodo al momento de mandarlo a imprimir seria un error fatal ya que nop existes una subclase//
    protected function recuperarContraseña(){
        return $this->recuperarContraseña; //Hacer una nueva contraseña//
    }

}
//Instanciacion de la clase
$obj = new CorreoElectronico();

//Mandar a imprimir//
echo "Mi correo es: " . $obj->correo();
echo $obj->contraseña();
echo "Mi nueva contraseña es: " . $obj->recuperarContraseña();





?>